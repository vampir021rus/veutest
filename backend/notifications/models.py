from django.db import models
from django.contrib.auth.models import User

class Notifications(models.Model):
    header = models.CharField(
        verbose_name = 'Заголовок',
        max_length = 200
    )
    content = models.TextField(
        verbose_name = 'Содержание',
        max_length = 1024
    )
    date = models.DateTimeField(verbose_name = 'Дата проведения')


    def __str__(self):
        return self.header
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from . import views

urlpatterns = [
    path('login', views.LoginView.as_view()),

    path('notifications-list', views.NotificationsListView.as_view()),
    path('notifications/<int:pk>', views.NotificationsDetailView.as_view()),  
]

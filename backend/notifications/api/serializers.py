from rest_framework import serializers
from rest_framework.authtoken.models import Token
from ..models import Notifications
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

class NotificationsSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format="%Y-%m-%d %H:%M")
    class Meta: 
        model=Notifications
        fields='__all__'
    
    def create(self, validated_data):
        return Notifications.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.header = validated_data.get('header', instance.header)
        instance.content = validated_data.get('content', instance.content)
        instance.date = validated_data.get('date', instance.date)
        instance.save()
        return instance

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=255, write_only=True)
    password = serializers.CharField(max_length=255, write_only=True)
    
    token = serializers.CharField(max_length=255, read_only=True)
    class Meta: 
        model=User
        fields = (
            'username',
            'password',
        )

    def validate(self, data):
        username = data.get('username', None)
        password = data.get('password', None)

        if not username:
            raise serializers.ValidationError('An name is required to login.')

        if not password:
            raise serializers.ValidationError('A password is required to log in.')

        user = authenticate(username=username, password=password)

        if not user:
            raise serializers.ValidationError('A user with this email and password was not found.')

        if not user.is_active:
            raise serializers.ValidationError('This user has been deactivated.')
        
        token, _ = Token.objects.get_or_create(user=user)

        return {
            'token': token.key,
        }
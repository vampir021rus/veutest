import json
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse

from rest_framework import status
from rest_framework import generics
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import NOT, AllowAny, IsAuthenticated, IsAdminUser
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin, CreateModelMixin

from notifications.models import Notifications
from notifications.api.serializers import NotificationsSerializer, LoginSerializer


class LoginView(APIView):
    permission_classes = [AllowAny]
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class NotificationsListView(ListModelMixin, CreateModelMixin, GenericAPIView):
    permission_classes = [AllowAny]
    serializer_class = NotificationsSerializer
    queryset = Notifications.objects.all()
    
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class NotificationsDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = NotificationsSerializer
    queryset = Notifications.objects.all()

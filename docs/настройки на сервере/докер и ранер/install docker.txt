установка докера
https://docs.docker.com/engine/install/ubuntu/

Install using the repository
1 обновимся и установим нужные пакеты
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

2 Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

проверк ключа, выполнив поиск по 8 последним символав
sudo apt-key fingerprint 0EBFCD88

3 Use the following command to set up the stable repository.
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
   
4 INSTALL DOCKER ENGINE
sudo apt-get install docker-ce docker-ce-cli containerd.io

5 устанавлвиаем определенную версию докера
sudo apt-get install docker-ce=5:18.09.1~3-0~ubuntu-xenial docker-ce-cli=5:18.09.1~3-0~ubuntu-xenial containerd.io

6 проверка докера
sudo docker run hello-world

crontab -e

# Очищать лишние docker образы в 6 утра
0 6 * * * docker image prune -f
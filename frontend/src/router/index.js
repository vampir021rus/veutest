import Vue from 'vue'
import Router from 'vue-router'
import main from '@/views/Main'
import contacts from '@/views/Contacts'
import notifications from '@/views/Notifications'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: main
    },
    {
      path: '/contacts',
      name: 'contacts',
      component: contacts
    },
    {
      path: '/notifications',
      name: 'notifications',
      component: notifications
    }
  ]
})

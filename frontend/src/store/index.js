import Vue from 'vue'
import Vuex from 'vuex'
import notifiications from './modules/notifiications'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    notifiications
  }
})

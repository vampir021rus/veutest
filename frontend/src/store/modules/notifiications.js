import Axios from 'axios'

var state = {
  notifiications: null
}

var getters = {
  NOTIFICATIONS: state => {
    return state.notifiications
  }
}

var mutations = {
  SET_NOTIFICATION: (state, items) => {
    state.notifiications = items
  },
  DELETE_NOTIFICATION: (state, id) => {
    let index = state.notifiications.findIndex(notifiications => notifiications.id === id)
    state.notifiications.splice(index, 1)
  },
  CREATE_NOTIFICATION: (state, item) => {
    state.notifiications.push(item)
  },
  UPDATE_NOTIFICATION: (state, item) => {
    let index = state.notifiications.findIndex(notifiications => notifiications.id === item.id)
    state.notifiications[index] = item
  }
}

var actions = {
  GET_NOTIFICATIONS: async (context, payload) => {
    try {
      let {data} = await Axios.get('/api/notifications/notifications-list')
      context.commit('SET_NOTIFICATION', data)
    } catch (err) {
      console.error(err)
    }
  },
  CREATE_NOTIFICATION: async (context, item) => {
    try {
      let {data} = await Axios.post('/api/notifications/notifications-list', item)
      if (data !== 400) {
        item['id'] = data['id']
        context.commit('CREATE_NOTIFICATION', item)
      }
    } catch (err) {
      console.error(err)
    }
  },
  DELETE_NOTIFICATION: async (context, item) => {
    try {
      let {data} = await Axios.delete('/api/notifications/notifications/' + item.id)
      if (data === '') {
        context.commit('DELETE_NOTIFICATION', item.id)
      }
    } catch (err) {
      console.error(err)
    }
  },
  UPDATE_NOTIFICATION: async (context, item) => {
    try {
      let {data} = await Axios.put('/api/notifications/notifications/' + item.id, item)
      if (data === 200) {
        context.commit('UPDATE_NOTIFICATION', item)
      }
    } catch (err) {
      console.error(err)
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

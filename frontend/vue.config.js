module.exports = {
    devServer: {
        hot: true,
        open: true,
        port: 8080,
        proxy: 'http://localhost:8000/'
    }
}
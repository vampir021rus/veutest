FROM python:3.8
ENV PYTHONUNBUFFERED 1

# указываем раб директорию 
WORKDIR /opt

# для кэширования requirements
COPY backend/requirements.txt .

# ставим зависимости requirements.txt
RUN pip install -r requirements.txt

# копируем бэкэнд в раб директорию
COPY backend ./

#даем права на старт
RUN chmod +x /opt/start.sh 

# стартуем проект в контейнере 
CMD /opt/start.sh
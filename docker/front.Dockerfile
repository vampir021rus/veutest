FROM alpine:latest as static

RUN apk add nodejs nodejs-npm build-base

WORKDIR /opt

COPY frontend/package.json ./
COPY frontend/package-lock.json ./
#RUN yarn add node-sass
RUN npm install --no-progress --no-interactive

COPY frontend ./
RUN npm run build --no-progress


FROM nginx:alpine

COPY docker/nginx/conf.d     /etc/nginx/conf.d
COPY docker/nginx/nginx.conf /etc/nginx/nginx.conf

COPY --from=static /opt/dist /www/dist
